import com.devcamp.Author;
import com.devcamp.Book;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Author author1 = new Author("Le Van Nam", "namlv@gmail.com");
        System.out.println(author1);

        Author author2 = new Author("Nguyen Thi Bac", "bacnt@gmail.com", 'f');
        System.out.println(author2);

        Book book1 = new Book("Java", author1, 200000);
        System.out.println(book1);

        Book book2 = new Book("HTML/CSS", author2, 150000, 15);
        System.out.println(book2);
    }
}
